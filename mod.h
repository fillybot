#include "shared.h"
#include <ctype.h>
#include <tdb.h>
#include <sys/time.h>

static inline char *token(char **foo, char sep)
{
	char *line = *foo, *ret;
	if (!line)
		return NULL;
	ret = line;
	while (*line && *line != sep)
		line++;
	if (*line) {
		*(line++) = 0;
		while (*line == sep)
			line++;
		*foo = *line ? line : NULL;
	} else
		*foo = NULL;
	return ret;
}

static inline unsigned getrand(void)
{
	unsigned r;
	if (RAND_bytes((unsigned char*)&r, sizeof(r)) > 0)
		return r;

	fprintf(stderr, "random number generator error: %s", ERR_error_string(ERR_peek_last_error(), NULL));
	while (ERR_get_error());
	return random();
}

struct command_hash
{
	char *string;
	void (*cmd)(struct bio *b, const char *nick, const char *host, const char *target, char *args);
	int admin;
	int enter, left;
	int64_t disabled_until;
	char failed_command[];
} __attribute__((aligned(256)));

static unsigned strhash(const char *h)
{
	TDB_DATA in = { .dptr = (char*)h, .dsize = strlen(h) };
	return tdb_jenkins_hash(&in);
}

static void __attribute__ ((__format__(__printf__, 3, 4))) privmsg(struct bio *b, const char *who, const char *fmt, ...)
{
	char *buffer = NULL;
	va_list va;

	va_start(va, fmt);
	vasprintf(&buffer, fmt, va);
	va_end(va);
	if (strchr(buffer, '\r') || strchr(buffer, '\n')) {
		fprintf(stderr, "Offending message: %s\n", buffer);
		free(buffer);
		assert(!"Invalid character in privmsg reply");
	} else {
		b->writeline(b, "PRIVMSG %s :%s", who, buffer);
		free(buffer);
	}
}

#define BUILD_BUG_ON(x) ((void)sizeof(char[1 - 2*!!(x)]))
#define privmsg(b, target, fmt, args...) do { \
	BUILD_BUG_ON(__builtin_strrchr( "\n" fmt, '\n') != __builtin_strchr("\n" fmt, '\n')); \
	privmsg(b, target, fmt, ##args); \
} while (0)

static void __attribute__ ((__format__(__printf__, 3, 4))) action(struct bio *b, const char *who, const char *fmt, ...)
{
	char *buffer = NULL;
	va_list va;

	va_start(va, fmt);
	vasprintf(&buffer, fmt, va);
	va_end(va);
	if (strchr(buffer, '\r') || strchr(buffer, '\n')) {
		fprintf(stderr, "Offending message: %s\n", buffer);
		free(buffer);
		assert(!"Invalid character in action reply");
	} else {
		b->writeline(b, "PRIVMSG %s :\001ACTION %s\001", who, buffer);
		free(buffer);
	}
}
#define action(b, who, fmt, args...) do { \
	BUILD_BUG_ON(__builtin_strrchr( "\n" fmt, '\n') != __builtin_strchr("\n" fmt, '\n')); \
	action(b, who, fmt, ##args); \
} while (0)

static int64_t get_time(struct bio *b, const char *target)
{
	struct timeval tv;
	if (gettimeofday(&tv, NULL) < 0) {
		static int complain_time;
		if (target && !complain_time++)
			privmsg(b, target, "Could not get time: %m");
		return -1;
	} else
		return tv.tv_sec;
}
