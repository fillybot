/* addr2line.c -- convert addresses to line number and function name
   Copyright 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006,
   2007, 2009  Free Software Foundation, Inc.
   Contributed by Ulrich Lauther <Ulrich.Lauther@mchp.siemens.de>

   This file is part of GNU Binutils.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, 51 Franklin Street - Fifth Floor, Boston,
   MA 02110-1301, USA. */

#include "core.h"

#include <bfd.h>

sigjmp_buf crash_buf;
static int crash_nsymbol;
static void *crash_pointers[0x100];
static char **crash_symbols;
static unsigned crash_signal;

static void crash_handler(int sig, siginfo_t *si, void *ptr)
{
	crash_nsymbol = backtrace(crash_pointers, sizeof(crash_pointers)/sizeof(void*));
	crash_signal = sig;
	siglongjmp(crash_buf, sig);
}

void set_signals(unsigned activate)
{
	int handle[] = { SIGABRT, SIGSEGV, SIGALRM };
	static struct sigaction oldsigs[sizeof(handle)/sizeof(*handle)];
	struct sigaction newsig;
	int i;
	newsig.sa_sigaction = crash_handler;
	sigemptyset(&newsig.sa_mask);
	newsig.sa_flags = SA_SIGINFO;
	if (!activate)
		alarm(0);
	for (i = 0; i < sizeof(handle)/sizeof(*handle); ++i) {
		if (activate)
			sigaction(handle[i], &newsig, oldsigs + i);
		else
			sigaction(handle[i], oldsigs+i, NULL);
	}
	if (activate)
		alarm(ALARM_TIME);
}

void crash_write_backtrace(struct bio *b, const char *nick)
{
	int i;
	b->writeline(b, "PRIVMSG %s :Signal %i", nick, crash_signal);
	for (i = 2; i < crash_nsymbol - 1; ++i)
		b->writeline(b, "PRIVMSG %s :[%i] = %s", nick, i-2, crash_symbols[i]);
}

void crash_mod(struct bio *b, const char *target)
{
	if (ponify)
		b->writeline(b, "PRIVMSG %s :{ Help! A%csistance is required! I believe there is something wrong with me! }", target, crash_signal == 6 ? 'S' : 's');
	else
		b->writeline(b, "PRIVMSG %s :Signal %i caught", target, crash_signal);
	free(crash_symbols);
	crash_symbols = backtrace_symbols(crash_pointers, crash_nsymbol);
}

void crash_init(void)
{
}

void crash_fini(void)
{
	free(crash_symbols);
	crash_symbols = NULL;
}
