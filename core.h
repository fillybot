#include "shared.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ucontext.h>
#include <sys/ioctl.h>
#include <execinfo.h>
#include <netdb.h>
#include <setjmp.h>

extern sigjmp_buf crash_buf;

void set_signals(unsigned activate);
void crash_write_backtrace(struct bio *b, const char *nick);
void crash_init(void);
void crash_fini(void);
void crash_mod(struct bio *b, const char *target);

extern unsigned ponify;
