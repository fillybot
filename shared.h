#define _GNU_SOURCE
#include <features.h>
#include <fcntl.h>
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <poll.h>
#include <stdarg.h>
#include <openssl/ssl.h>
#include <openssl/rand.h>
#include <openssl/err.h>

#include "owner.h"

#define ALARM_TIME 240

static inline unsigned admin(const char *ident, const char *host)
{
	if (!host)
		return 0;
	return !strcmp(host, "equestria") || // un1c0rn
	       !strcmp(host, "friendship.sgu.equ") || // robomint
	       !strcmp(host, "genres.i.dont.even") || // z0r8
	       !strcmp(host, "unaffiliated/apologue") || // foxeee
	       !strcmp(host, "The.Gambler") || // Hope
	       !strcmp(host, "HaveConviction-WillTravel") || // Hope
	       !strcmp(host, "unaffiliated/metool") || // Hope
	       !strcmp(host, "Metool.user.gamesurge") || // Hope
	       (!strcmp(ident, "sid18823") && strstr(host, "gateway/web/irccloud.com/")) || // Hope
	       !strcmp(host, "who.needs.pants.anyway") || // foxie
	       !strcmp(host, "assassin.for.hire") || // foxie
	       !strcmp(host, "bookworm.extraordinaire") || // foxie
	       !strcmp(host, "the_Library") || // Foxie canternet
	       !strcmp(host, "sweetie.belle") || // self
	       strcasestr(host, owner); // un1c0rn on other network
}

struct bio {
	void (*writeline)(struct bio *b, const char *fmt, ...)
	                  __attribute__ ((__format__ (__printf__, 2, 3)));
	int (*fill)(struct bio *b);
	int (*readline)(struct bio *b, char **line);
	int (*poll)(struct bio *b, int timeout);
	SSL *ssl;
	int fd, maxlen, len, ofs;
	char *priv;
	long err;
};

void init_hook(struct bio *b, const char *target, const char *self, unsigned ponify);

void privmsg_hook(struct bio *b, const char *prefix, const char *ident, const char *host,
                  const char *const *args, unsigned nargs);

void command_hook(struct bio *b, const char *prefix, const char *ident, const char *host,
                  const char *command, const char *const *args, unsigned nargs);
