#include "core.h"
#include <dlfcn.h>
#include <getopt.h>

static char nick_self[128];
static char *channels[128];
static unsigned nchannel;
static char *password;
static char *wanted_nick;

static void *dl_mod;
static typeof(init_hook) *init_ptr;
static typeof(privmsg_hook) *privmsg_ptr;
static typeof(command_hook) *command_ptr;

static int reload_requested = 1;
unsigned ponify;

static void reload_handler(int signal)
{
	reload_requested = 1;
}

static void handle_connect(struct bio *b, const char *prefix, const char *ident, const char *host,
                           const char *const *args, unsigned nargs)
{
	int i;
	strncpy(nick_self, args[0], sizeof(nick_self)-1);
	b->writeline(b, "NS :identify %s %s", wanted_nick, password);
	b->writeline(b, "MODE %s +x", nick_self);
	sleep(1);
	for (i = 0; i < nchannel; ++i)
		b->writeline(b, "JOIN %s", channels[i]);
}

static void handle_nick(struct bio *b, const char *prefix, const char *ident, const char *host,
                        const char *const *args, unsigned nargs)
{
	if (!strcmp(nick_self, prefix)) {
		strncpy(nick_self, args[0], sizeof(nick_self)-1);
		return;
	}
	if (!strcasecmp(args[0], "SweetieBelle") || !strcasecmp(args[0], wanted_nick))
		b->writeline(b, "NS :ghost %s %s", args[0], password);
	if (dl_mod && command_ptr)
		command_ptr(b, prefix, ident, host, "NICK", args, nargs);
}

static void handle_ping(struct bio *b, const char *prefix, const char *ident, const char *host,
                        const char *const *args, unsigned nargs)
{
	b->writeline(b, "PONG %s", args[0]);
}

static void mod_unload(struct bio *b, const char *target)
{
	reload_requested = 0;
	if (dlclose(dl_mod)) {
		const char *foo = strchr(dlerror(), ':');
		if (foo)
			foo += 2;
		b->writeline(b, "PRIVMSG %s :UNLOAD ERROR: %s", target, foo ? foo : dlerror());
	}
	dl_mod = NULL;
	privmsg_ptr = NULL;
	command_ptr = NULL;
}

static void mod_load(struct bio *b, const char *target)
{
	if (dl_mod)
		mod_unload(b, target);
	reload_requested = 0;
	dl_mod = dlopen("mod.so", RTLD_LOCAL|RTLD_NOW);
	if (!dl_mod) {
		const char *foo = strchr(dlerror(), ':');
		if (foo)
			foo += 2;
		b->writeline(b, "PRIVMSG %s :LOAD ERROR: %s", target, foo ? foo : dlerror());
	}

	init_ptr = dlsym(dl_mod, "init_hook");
	privmsg_ptr = dlsym(dl_mod, "privmsg_hook");
	command_ptr = dlsym(dl_mod, "command_hook");
	if (init_ptr)
		init_ptr(b, target, nick_self, ponify);
	else
		b->writeline(b, "PRIVMSG %s :I feel smarter", target);
}

static void handle_privmsg(struct bio *b, const char *prefix, const char *ident, const char *host,
                           const char *const *args, unsigned nargs)
{
	const char *target;
	if (!ident || !host)
		return;
	target = args[0][0] == '#' ? args[0] : prefix;
	if (admin(ident, host)) {
		if (!strcmp(args[1], "crash")) {
			*(char *)0 = 0;
			return;
		} else if (!strcmp(args[1], "load")) {
			mod_load(b, target);
			return;
		} else if (!strcmp(args[1], "unload")) {
			if (!dl_mod)
				return;
			mod_unload(b, target);
			b->writeline(b, "PRIVMSG %s :{ I feel different. }", target);
			return;
		}
	}
	if (!strcmp(args[1], "bt")) {
		crash_write_backtrace(b, prefix);
		return;
	}

	if (dl_mod && privmsg_ptr)
		privmsg_ptr(b, prefix, ident, host, args, nargs);
	if (reload_requested)
		mod_load(b, target);
}

struct command {
	const char *cmd;
	unsigned min_args;
	unsigned prefixed;
	void (*ptr)(struct bio *b, const char *prefix, const char *ident, const char *host,
	            const char *const *args, unsigned nargs);
	unsigned builtin;
} cmds[] = {
	{ "PRIVMSG", 2, 1, handle_privmsg },
	{ "NICK", 1, 1, handle_nick },
	{ "PING", 1, 0, handle_ping, 1 },
	{ "001", 2, 0, handle_connect, 1 },
	{}
};

static void ssl_writeline(struct bio *b, const char *fmt, ...)
                          __attribute__ ((__format__ (__printf__, 2, 3)));

static void plain_writeline(struct bio *b, const char *fmt, ...)
                            __attribute__ ((__format__ (__printf__, 2, 3)));

static void bio_seterr(struct bio *b, ssize_t err)
{
	if (b->err == 0)
		b->err = errno;
}

static void bio_fill(struct bio *b, ssize_t avail)
{
	if (b->maxlen < avail + b->len) {
		memmove(b->priv, b->priv + b->ofs, b->len - b->ofs);
		b->len -= b->ofs;
		b->ofs = 0;
		if (b->maxlen < b->len + avail) {
			b->priv = realloc(b->priv, b->len + avail);
			b->maxlen = b->len + avail;
		}
	}
}

static int plain_fill(struct bio *b)
{
	ssize_t avail = 0;
	if (ioctl(b->fd, FIONREAD, &avail) < 0) {
		bio_seterr(b, -errno);
		return -errno;
	}
	bio_fill(b, avail);
	avail = read(b->fd, b->priv + b->len, avail);
	b->len += avail;
	return avail;
}

static int ssl_fill(struct bio *b)
{
	ssize_t avail;
	int ret;

	bio_fill(b, 1);
	ret = SSL_read(b->ssl, b->priv + b->len, 1);
	if (ret <= 0)
		return 0;
	b->len += ret;

	avail = SSL_pending(b->ssl);
	if (avail <= 0)
		return 0;
	bio_fill(b, avail);
	avail = SSL_read(b->ssl, b->priv + b->len, avail);
	if (avail <= 0)
		return 0;
	b->len += avail;
	return avail;
}

static int bio_poll(struct bio *b, int timeout)
{
	int ret;
	struct pollfd pfd = {
		.fd = b->fd,
		.events = POLLIN|POLLRDHUP|POLLHUP
	};
	ret = poll(&pfd, 1, timeout);
	if (ret && pfd.revents & ~POLLIN)
		return -1;
	return ret;
}

static int bio_readline(struct bio *b, char **line)
{
	int i;
	*line = NULL;
	for (i = b->ofs; i < b->len; ++i) {
		if (b->priv[i] == 0)
			b->priv[i] = '0';
		if (b->priv[i] != '\r' && b->priv[i] != '\n')
			continue;
		if (i != b->ofs)
			break;
		else
			b->ofs++;
	}
	if (b->ofs == b->len) {
		b->len = b->ofs = 0;
		return 0;
	}
	if (i == b->len)
		return 0;
	*line = b->priv + b->ofs;
	i -= b->ofs;
	(*line)[i] = 0;
	if (i == b->len-1)
		b->len = b->ofs = 0;
	else
		b->ofs += i + 1;
	return i;
}

static void plain_writeline(struct bio *b, const char *fmt, ...)
{
	char *buffer, *ofs;
	ssize_t ret, rem;
	va_list va;
	va_start(va, fmt);
	ret = vasprintf(&buffer, fmt, va);
	va_end(va);
	if (ret <= 0) {
		bio_seterr(b, ret);
		return;
	}
	buffer[ret] = '\n'; /* Replace \0 with \n */
	rem = ret+1;
	ofs = buffer;
	do {
		ret = write(b->fd, ofs, rem);
		if (ret < 0)
			break;
		ofs += ret;
		rem -= ret;
	} while (rem);
	free(buffer);
}

static void ssl_writeline(struct bio *b, const char *fmt, ...)
{
	char *buffer, *ofs;
	ssize_t ret, rem;
	va_list va;

	va_start(va, fmt);
	ret = vasprintf(&buffer, fmt, va);
	va_end(va);
	if (ret <= 0) {
		bio_seterr(b, ret);
		return;
	}
	buffer[ret] = '\n'; /* Replace \0 with \n */
	rem = ret+1;
	ofs = buffer;

	do {
		ret = SSL_write(b->ssl, ofs, rem);
		if (ret <= 0) {
			bio_seterr(b, SSL_get_error(b->ssl, ret));
			break;
		}
		ofs += ret;
		rem -= ret;
	} while (rem);
	free(buffer);
}

static int get_connection(const char *server, const char *port)
{
	int sfd, s;
	struct addrinfo hints, *result, *rp;

	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_UNSPEC; /* Allow IPv4 or IPv6 */
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = 0;
	hints.ai_protocol = 0;

	s = getaddrinfo(server, port, &hints, &result);
	if (s != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
		return -1;
	}

	for (rp = result; rp != NULL; rp = rp->ai_next) {
		sfd = socket(rp->ai_family, rp->ai_socktype,
                             rp->ai_protocol);
		if (sfd == -1)
			continue;

		if (connect(sfd, rp->ai_addr, rp->ai_addrlen) != -1)
			break;                  /* Success */

		close(sfd);
	}

	if (rp == NULL) {
		fprintf(stderr, "Could not connect\n");
		return -1;
	}

	freeaddrinfo(result);
	return sfd;
}

static char *token(char **foo)
{
	char *line = *foo, *ret;
	ret = line;
	while (*line && *line != ' ')
		line++;
	if (*line) {
		*(line++) = 0;
		while (*line == ' ')
			line++;
		*foo = line;
	} else
		*foo = NULL;
	return ret;
}

static inline void handle_line(struct bio *b, char *line)
{
	char *prefix = NULL, *command, *ident = NULL, *host = NULL;
	const char *args[128];
	int i, nargs = 0;
	if (line[0] == ':') {
		line++;
		prefix = token(&line);
		ident = strchr(prefix, '!');
		if (ident) {
			*(ident++) = 0;
			host = strchr(ident, '@');
			if (host)
				*(host++) = 0;
		}
		if (!line)
			return;
	}
	command = token(&line);

	while (line && nargs < sizeof(args)/sizeof(*args)) {
		if (line[0] == ':') {
			args[nargs++] = ++line;
			break;
		}
		args[nargs++] = token(&line);
	}
	for (i = 0; i < sizeof(cmds)/sizeof(*cmds)-1; ++i) {
		typeof(*cmds) *c = cmds+i;
		if (strcasecmp(c->cmd, command))
			continue;
		if (nargs < c->min_args) {
			fprintf(stderr, "Dropped %s because of %u < %u\n", command, nargs, c->min_args);
			continue;
		}
		if (!prefix && c->prefixed) {
			fprintf(stderr, "Dropped %s because not prefixed\n", command);
			continue;
		}
		if (!c->builtin) {
			int sig = sigsetjmp(crash_buf, 1);
			if (!sig) {
				set_signals(1);
				c->ptr(b, prefix, ident, host, args, nargs);
			}
			set_signals(0);
			if (sig)
				crash_mod(b, channels[0]);
		} else
			c->ptr(b, prefix, ident, host, args, nargs);
		break;
	}
	if (!cmds[i].cmd && dl_mod && command_ptr) {
		int sig = setjmp(crash_buf);
		if (!sig) {
			set_signals(1);
			command_ptr(b, prefix, ident, host, command, args, nargs);
		}
		set_signals(0);
		if (sig)
			crash_mod(b, channels[0]);
	}
}

static void mainloop(struct bio *b, const char *server)
{
	int ret = 0, pinged = 0;
	b->writeline(b, "NICK %s", wanted_nick);
	if (ponify)
		b->writeline(b, "USER %s localhost %s :{ Cutie Mark Acquisition Program }", wanted_nick, server);
	else
		b->writeline(b, "USER %s localhost %s :Robotic overlord", wanted_nick, server);
	while (ret >= 0) {
		char *line;
		ret = b->poll(b, pinged ? 50000 : 250000);
		if (!ret) {
			if (pinged)
				break;
			b->writeline(b, "PING %s", nick_self);
			pinged = 1;
			continue;
		}
		pinged = 0;
		if (ret > 0)
			ret = b->fill(b);
		if (ret <= 0)
			continue;
		while ((ret = b->readline(b, &line) > 0))
			handle_line(b, line);
	}
}

static void usage(char *prog, FILE *out)
{
	fprintf(out, "Usage: %s\n", prog);
	fprintf(out, "\t-s\t --server <server>\n");
	fprintf(out, "\t-p\t --port <port>\n");
	fprintf(out, "\t-P\t --password <password>\n");
	fprintf(out, "\t-c\t --channel <channel>\n");
	fprintf(out, "\t-n\t --nick <nick>\n");
	fprintf(out, "\t-S\t --ssl\n");
}

int main(int argc, char *argv[])
{
	char *server = NULL, *port = NULL;
	int use_ssl = 0, c, option_index;
	SSL_CTX *ssl_ctx = NULL;
	SSL *ssl = NULL;
	struct option options[] = {
		{ "server", required_argument, NULL, 's' },
		{ "port", required_argument, NULL, 'p' },
		{ "password", required_argument, NULL, 'P' },
		{ "channel", required_argument, NULL, 'c' },
		{ "nick", required_argument, NULL, 'n' },
		{ "ssl", no_argument, NULL, 'S' },
		{ "help", no_argument, NULL, 'h' },
		{ 0, 0, 0, 0 }
	};
	ponify = 0;
	while ((c = getopt_long(argc, argv, "s:p:P:c:n:Sh", options, &option_index)) >= 0) {
		switch (c) {
		case 'c': channels[nchannel++] = optarg; break;
		case 's': server = optarg; break;
		case 'p': port = optarg; break;
		case 'P': password = optarg; break;
		case 'n': wanted_nick = optarg; break;
		case 'S': use_ssl = 1; break;
		case 'h': usage(argv[0], stdout); return 0;
		default: usage(argv[0], stderr); return -1;
		}
		if (optarg) {
			ponify |= !!strcasestr(optarg, "pony");
			ponify |= !!strcasestr(optarg, "bron");
		}
	}
	crash_init();
	if (!server || !port || !nchannel || !wanted_nick || !password) {
		usage(argv[0], stderr);
		return -1;
	}
	if (use_ssl) {
		SSL_library_init();
		ssl_ctx = SSL_CTX_new(SSLv3_client_method());
		SSL_CTX_set_verify(ssl_ctx, SSL_VERIFY_NONE, NULL);
	}
	signal(SIGUSR1, reload_handler);
	while (1) {
		int fd = get_connection(server, port);
		if (fd < 0) {
			sleep(5);
			continue;
		}
		if (use_ssl) {
			int ssl_err;
			ssl = SSL_new(ssl_ctx);
			if (!ssl)
				break;
			ssl_err = SSL_set_fd(ssl, fd);
			if (ssl_err <= 0)
				goto retry;

			ssl_err = SSL_get_error(ssl, SSL_connect(ssl));
			if (ssl_err != SSL_ERROR_NONE) {
				fprintf(stderr, "error code: %i\n", ssl_err);
				goto retry;
			}
			struct bio b = {
				.readline = bio_readline,
				.writeline = ssl_writeline,
				.poll = bio_poll,
				.fill = ssl_fill,
				.ssl = ssl,
				.fd = fd
			};
			mainloop(&b, server);
			SSL_shutdown(ssl);
			free(b.priv);
retry:
			SSL_free(ssl);
		} else {
			struct bio b = {
				.readline = bio_readline,
				.writeline = plain_writeline,
				.poll = bio_poll,
				.fill = plain_fill,
				.fd = fd
			};
			mainloop(&b, server);
			free(b.priv);
		}
		close(fd);
		sleep(3);
	}
	if (use_ssl)
		SSL_CTX_free(ssl_ctx);
	crash_fini();
	return 0;
}
