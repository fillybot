#include "mod.h"

#include <libxml/xmlstring.h>
#include <libxml/HTMLparser.h>

#include <stdint.h>
#include <inttypes.h>
#include <sys/wait.h>
#include <curl/curl.h>

#include <magic.h>

#include "entities.h"
#include "quotes.h"

static const char *nick_self;
static unsigned ponify;
#define elements(x) (sizeof(x)/sizeof(*x))

static struct tdb_context *feed_db, *chan_db, *mail_db, *seen_db;

static struct command_hash command_channel = { "#", NULL };

#define sstrncmp(a, b) (strncmp(a, b, sizeof(b)-1))
#define sstrncasecmp(a, b) (strncasecmp(a, b, sizeof(b)-1))

#define SPAM_CUTOFF 5

static int pipe_command(struct bio *b, const char *target, const char *nick, int redirect_stdout, int redirect_stderr, char *argv[])
{
	int fd[2];
	int lines = 0;
	if (pipe2(fd, O_CLOEXEC) < 0) {
		privmsg(b, target, "Could not create pipe: %m");
		return -1;
	}
	pid_t pid = fork();
	if (pid < 0) {
		privmsg(b, target, "Could not fork: %m");
		close(fd[0]);
		close(fd[1]);
		return -1;
	} else if (!pid) {
		int fdnull = open("/dev/null", O_WRONLY|O_CLOEXEC);
		close(fd[0]);
		if (dup3(redirect_stdout ? fd[1] : fdnull, 1, 0) < 0)
			exit(errno);
		if (dup3(redirect_stderr ? fd[1] : fdnull, 2, 0) < 0)
			exit(errno);
		exit(execv(argv[0], argv));
	} else {
		int loc = -1;
		char buffer[0x100];
		int bufptr = 0, ret;
		close(fd[1]);
		fcntl(fd[0], F_SETFL, O_NONBLOCK);
		while ((ret = waitpid(pid, &loc, WNOHANG)) >= 0) {
			while (read(fd[0], buffer+bufptr, 1) == 1) {
				if (buffer[bufptr] != '\n' && bufptr < sizeof(buffer)-1) {
					bufptr++;
					continue;
				} else if (bufptr) {
					buffer[bufptr] = 0;
					bufptr = 0;
					lines++;
					if (lines < SPAM_CUTOFF)
						privmsg(b, nick, "%s", buffer);
					else
						fprintf(stderr, "%s\n", buffer);
				}
			}

			if (ret) {
				if (bufptr) {
					buffer[bufptr] = 0;
					if (lines < SPAM_CUTOFF)
						privmsg(b, nick, "%s", buffer);
					else
						fprintf(stderr, "%s\n", buffer);
				}
				if (lines >= SPAM_CUTOFF)
					privmsg(b, nick, "%i lines suppressed", lines - SPAM_CUTOFF + 1);
				break;
			}
		}
		if (ret < 0)
			privmsg(b, target, "error on waitpid: %m");
		else
			ret = loc;
		close(fd[0]);
		return ret;
	}
}

#include "local.c"

static struct command_hash commands[2048];

static void command_abort(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	abort();
	return;
}

static void command_crash(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	*(char*)0 = 0;
}

static void command_coinflip(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	if (getrand() & 1)
		action(b, target, "whirrrs and clicks excitedly at %s", nick);
	else
		action(b, target, "eyes %s as nothing happens", nick);
}

static void command_shesaid(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	privmsg(b, target, "%s: \"%s\"", args ? args : nick, women_quotes[getrand() % elements(women_quotes)]);
}

static void squash(char *title)
{
	char *start = title;
	goto start;
	while (*title) {
		while (*title != '\n' && *title != '\r' && *title != '\t' && *title != ' ' && *title) {
			*(start++) = *(title++);
		}
		if (*title)
			*(start++) = ' ';
start:
		while (*title == '\n' || *title == '\r' || *title == '\t' || *title == ' ')
			title++;
	}
	*start = 0;
}

struct curl_download_context
{
	char *data;
	size_t len;
};

static size_t write_data(void *ptr, size_t size, size_t nmemb, void *member) {
	struct curl_download_context *ctx = member;
	size *= nmemb;
	ctx->data = realloc(ctx->data, ctx->len + size);
	memcpy(ctx->data + ctx->len, ptr, size);
	ctx->len += size;
	return size;
}

static const char *get_text(xmlNode *cur)
{
	for (; cur; cur = cur->next) {
		if (cur->type == XML_TEXT_NODE)
			return cur->content;
	}
	return NULL;
}

static const char *get_link(xmlAttr *cur, const char *which)
{
	for (; cur; cur = cur->next) {
		if (!strcasecmp(cur->name, which))
			return get_text(cur->children);
	}
	return NULL;
}

static int get_feed_entry(struct bio *b, const char *nick, const char *target, const char *category, xmlNode *entry, const char **title, const char **link)
{
	const char *cur_title = NULL, *cur_link = NULL;
	xmlNode *cur;
	int cur_match = !category;
	for (cur = entry->children; cur; cur = cur->next) {
		const char *name = cur->name, *cur_cat;
		if (cur->type != XML_ELEMENT_NODE)
			continue;
		else if (!strcasecmp(name, "link")) {
			const char *ishtml = get_link(cur->properties, "type");
			const char *rel = get_link(cur->properties, "rel");
			if ((!ishtml || !strcasecmp(ishtml, "text/html")) &&
			    (!rel || !strcasecmp(rel, "alternate")))
				cur_link = get_link(cur->properties, "href");
		} else if (!strcasecmp(name, "title"))
			cur_title = get_text(cur->children);
		else if (!cur_match && !strcasecmp(name, "category") &&
			 (cur_cat = get_link(cur->properties, "term")) &&
			 strcasestr(cur_cat, category))
			cur_match = 1;
	}

	if (cur_title)
		*title = cur_title;
	else
		*title = "<no title>";
	*link = cur_link;
	return !cur_link  ? -1 : cur_match;
}

static const char *walk_feed(struct bio *b, const char *nick, const char *target, const char *url, const char *category, xmlNode *root, const char *last_link)
{
	const char *main_title = NULL, *main_subtitle = NULL, *main_link = NULL, *title, *link, *prev_link = NULL, *prev_title = NULL;
	char *t;
	int match, updates = 0;
	xmlNode *cur, *entry = NULL;
	for (cur = root->children; cur; cur = cur->next) {
		const char *name = cur->name;
		if (cur->type != XML_ELEMENT_NODE)
			continue;
		else if (!strcasecmp(name, "category"))
			continue;
		else if (!strcasecmp(name, "entry"))
			entry = entry ? entry : cur;
		else if (!strcasecmp(name, "title"))
			main_title = get_text(cur->children);
		else if (!strcasecmp(name, "subtitle"))
			main_subtitle = get_text(cur->children);
		else if (!strcasecmp(name, "link")) {
			const char *ishtml = get_link(cur->properties, "type");
			const char *rel = get_link(cur->properties, "rel");
			if ((!ishtml || !strcasecmp(ishtml, "text/html")) && (!rel || !strcasecmp(rel, "alternate")))
				main_link = get_link(cur->properties, "href");
		}
	}
	if (!main_link || !main_title) {
		privmsg(b, target, "%s: Failed to parse main: %s %s", nick, main_link, main_title);
		return NULL;
	}
	if (!entry)
		return NULL;

	match = get_feed_entry(b, nick, target, category, entry, &title, &link);
	for (; !match && entry; entry = entry->next) {
		if (!strcasecmp(entry->name, "entry"))
			match = get_feed_entry(b, nick, target, category, entry, &title, &link);
	}

	if (match < 0)
		return NULL;
	if (!last_link) {
		if (link) {
			if (main_subtitle)
				privmsg(b, target, "adding feed %s \"%s\": %s", main_link, main_title, main_subtitle);
			else
				privmsg(b, target, "adding feed %s \"%s\"", main_link, main_title);
		}
		if (match > 0) {
			t = strdup(title);
			squash(t);
			privmsg(b, target, "Most recent entry: %s %s", link, t);
			free(t);
		} else
			privmsg(b, target, "Currently having no entries for this feed that matches the filter");
		return link;
	}
	if (!strcmp(last_link, link) || !match || !entry)
		return link;

	for (entry = entry->next; entry; entry = entry->next) {
		const char *cur_link, *cur_title;
		if (strcasecmp(entry->name, "entry"))
			continue;
		match = get_feed_entry(b, nick, target, category, entry, &cur_title, &cur_link);
		if (match < 0 || !strcmp(last_link, cur_link))
			break;
		if (match) {
			prev_link = cur_link;
			prev_title = cur_title;
			updates++;
		}
	}
	if (updates == 1) {
		t = strdup(prev_title);
		squash(t);
		privmsg(b, target, "( %s ): %s", prev_link, t);
		free(t);
	} else if (updates > 1)
		privmsg(b, target, "( %s ): %u updates, linking most recent", main_link, 1+updates);
	t = strdup(title);
	squash(t);
	privmsg(b, target, "( %s ): %s", link, t);
	free(t);
	return link;
}

static const char *walk_rss(struct bio *b, const char *nick, const char *target, const char *url, xmlNode *root, const char *last_link)
{
	const char *main_title = NULL, *main_link = NULL, *title = NULL, *link = NULL, *ver;
	char *t;
	xmlNode *cur, *entry = NULL;
	ver = get_link(root->properties, "version");
	if (!ver || strcmp(ver, "2.0")) {
		if (!ver)
			privmsg(b, target, "%s: Could not parse rss feed", nick);
		else
			privmsg(b, target, "%s: Invalid rss version \"%s\"", nick, ver);
		return NULL;
	}
	for (cur = root->children; cur && cur->type != XML_ELEMENT_NODE; cur = cur->next);
	if (!cur)
		return NULL;
	for (cur = cur->children; cur; cur = cur->next) {
		const char *name = cur->name;
		if (cur->type != XML_ELEMENT_NODE)
			continue;
		if (!strcasecmp(name, "title"))
			main_title = get_text(cur->children);
		else if (!strcasecmp(name, "link"))
			main_link = main_link ? main_link : get_text(cur->children);
		else if (!strcasecmp(name, "item"))
			entry = entry ? entry : cur;
	}
	if (!main_link || !main_title) {
		privmsg(b, target, "%s: Failed to parse main: %s %s", nick, main_link, main_title);
		return NULL;
	}
	if (!entry)
		return NULL;

	link = title = NULL;
	for (cur = entry->children; cur; cur = cur->next) {
		const char *name = cur->name;
		if (cur->type != XML_ELEMENT_NODE)
			continue;
		if (!strcasecmp(name, "title"))
			title = get_text(cur->children);
		else if (!strcasecmp(name, "link") && !link)
			link = get_text(cur->children);
	}
	if (!title)
		title = "<no title>";
	if (!link) {
		privmsg(b, target, "%s: Failed to parse entry: %s %s", nick, link, title);
		return NULL;
	}
	if (!last_link) {
		t = strdup(title);
		privmsg(b, target, "adding feed %s \"%s\"", main_link, main_title);
		squash(t);
		privmsg(b, target, "Most recent entry: %s %s", link, t);
		free(t);
	} else if (strcmp(last_link, link)) {
		int updates = 0;
		const char *prev_title = NULL, *prev_link = NULL, *cur_title = NULL, *cur_link = NULL;
		for (entry = entry->next; entry; entry = entry->next) {
			if (strcasecmp(entry->name, "item"))
				continue;
			prev_title = cur_title;
			prev_link = cur_link;
			cur_title = cur_link = NULL;
			for (cur = entry->children; cur; cur = cur->next) {
				const char *name = cur->name;
				if (cur->type != XML_ELEMENT_NODE)
					continue;
				if (!strcasecmp(name, "title"))
					cur_title = get_text(cur->children);
				else if (!strcasecmp(name, "link") && !cur_link)
					cur_link = get_text(cur->children);
			}
			if (!cur_title)
				cur_title = "<no title>";
			if (!cur_link || !strcmp(last_link, cur_link))
				break;
			updates++;
		}
		if (updates == 1) {
			t = strdup(prev_title);
			squash(t);
			privmsg(b, target, "( %s ): %s", prev_link, t);
			free(t);
		} else if (updates > 1)
			privmsg(b, target, "( %s ): %u updates, linking most recent", main_link, 1+updates);
		t = strdup(title);
		squash(t);
		privmsg(b, target, "( %s ): %s", link, t);
		free(t);
	}
	return link;
}

// HTML is a mess, so I'm just walking the tree depth first until I find the next element..
static xmlNode *next_link(xmlNode *cur_node)
{
	if (cur_node->children)
		return cur_node->children;
	while (cur_node) {
		if (cur_node->next)
			return cur_node->next;
		cur_node = cur_node->parent;
	}
	return NULL;
}

static const char *get_atom_link(xmlNode *cur)
{
	for (; cur; cur = next_link(cur)) {
		if (cur->type != XML_ELEMENT_NODE)
			continue;
		if (!strcasecmp(cur->name, "link")) {
			const char *isxml = get_link(cur->properties, "type");
			if (isxml && !strcasecmp(isxml, "application/atom+xml"))
				return get_link(cur->properties, "href");
		}
	}
	return NULL;
}

static const char *get_rss_link(xmlNode *cur)
{
	for (; cur; cur = next_link(cur)) {
		if (cur->type != XML_ELEMENT_NODE)
			continue;
		if (!strcasecmp(cur->name, "link")) {
			const char *isxml = get_link(cur->properties, "type");
			if (isxml && !strcasecmp(isxml, "application/rss+xml"))
				return get_link(cur->properties, "href");
		}
	}
	return NULL;
}

static void do_html(struct bio *b, const char *nick, const char *target, const char *url, const char *data, unsigned len)
{
	htmlDocPtr ctx = htmlReadMemory(data, len, 0, url, HTML_PARSE_RECOVER|HTML_PARSE_NOERROR|HTML_PARSE_NOWARNING);
	xmlNode *root = xmlDocGetRootElement(ctx);
	const char *link = get_atom_link(root);
	if (link)
		privmsg(b, target, "%s: not a valid feed link, try atom: %s", nick, link);
	else if ((link = get_rss_link(root)))
		privmsg(b, target, "%s: not a valid feed link, try rss: %s", nick, link);
	else
		privmsg(b, target, "%s: not a valid feed link, no suggestion found", nick);
	xmlFreeDoc(ctx);
}

static size_t get_time_from_header(void *data, size_t size, size_t size2, void *ptr)
{
	char *d, *e;
	size *= size2;
	if (sstrncmp(data, "Last-Modified: "))
		return size;
	data += sizeof("Last-Modified: ")-1;
	*(char**)ptr = d = strdup(data);
	if ((e = strchr(d, '\r')))
		*e = 0;
	return size;
}

static int check_single_feed(struct bio *b, const char *target, TDB_DATA key, const char *last_modified, const char *url, const char *link, const char *nick)
{
	struct curl_download_context curl_ctx = {};
	struct curl_slist *headers = NULL;
	char error[CURL_ERROR_SIZE], *category = strchr(url, '#');
	int retval = -1;
	headers = curl_slist_append(headers, "User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:9.0.1) Gecko/20100101 Firefox/9.0.1");
	headers = curl_slist_append(headers, "Accept: */*");
	if (category)
		*(category++) = 0;

	CURL *h = curl_easy_init();
	curl_easy_setopt(h, CURLOPT_HTTPHEADER, headers);
	curl_easy_setopt(h, CURLOPT_URL, url);
	curl_easy_setopt(h, CURLOPT_WRITEFUNCTION, write_data); 
	curl_easy_setopt(h, CURLOPT_WRITEDATA, &curl_ctx); 
	curl_easy_setopt(h, CURLOPT_ERRORBUFFER, error);
	curl_easy_setopt(h, CURLOPT_FOLLOWLOCATION, 1);
	curl_easy_setopt(h, CURLOPT_MAXREDIRS, 3);
	curl_easy_setopt(h, CURLOPT_SSL_VERIFYPEER, 0);
	curl_easy_setopt(h, CURLOPT_TIMEOUT, 8);
	curl_easy_setopt(h, CURLOPT_CONNECTTIMEOUT, 8);
	curl_easy_setopt(h, CURLOPT_FILETIME, 1);
	curl_easy_setopt(h, CURLOPT_HEADERFUNCTION, get_time_from_header);
	curl_easy_setopt(h, CURLOPT_WRITEHEADER, &last_modified);
	//curl_easy_setopt(h, CURLOPT_VERBOSE, 1);

	if (last_modified) {
		char *tmp;
		asprintf(&tmp, "If-Modified-Since: %s", last_modified);
		headers = curl_slist_append(headers, tmp);
		free(tmp);
	}

	int success = curl_easy_perform(h);
	curl_slist_free_all(headers);
	if (success == CURLE_OK) {
		char *mime = NULL;
		long code;
		curl_easy_getinfo(h, CURLINFO_CONTENT_TYPE, &mime);
		curl_easy_getinfo(h, CURLINFO_RESPONSE_CODE, &code);
		if (code == 304)
			{ }
		else if (!mime ||
			 !sstrncmp(mime, "application/xml") ||
			 !sstrncmp(mime, "text/xml") ||
			 !sstrncmp(mime, "application/rss+xml") ||
			 !sstrncmp(mime, "application/atom+xml")) {
			const char *ret_link = NULL;
			xmlDocPtr ctx = xmlReadMemory(curl_ctx.data, curl_ctx.len, 0, url, XML_PARSE_NOWARNING | XML_PARSE_NOERROR);
			xmlNode *root = xmlDocGetRootElement(ctx);

			if (!root || !root->name)
				fprintf(stderr, "Failed to parse feed %s %p", url, root);
			else if (!strcasecmp(root->name, "feed"))
				ret_link = walk_feed(b, nick, target, url, category, root, link);
			else if (!strcasecmp(root->name, "rss"))
				ret_link = walk_rss(b, nick, target, url, root, link);
			else {
				privmsg(b, target, "Unknown feed type \"%s\"", root->name);
				goto free_ctx;
			}
			if (category)
				category[-1] = '#';

			if (ret_link && (!link || strcmp(ret_link, link))) {
				TDB_DATA val;
				asprintf((char**)&val.dptr, "%s\001%s", last_modified ? last_modified : "", ret_link);
				val.dsize = strlen(val.dptr)+1;
				if (tdb_store(feed_db, key, val, 0) < 0)
					privmsg(b, target, "updating returns %s", tdb_errorstr(feed_db));
				free(val.dptr);
				retval = 1;
			}
			else
				retval = 0;

free_ctx:
			xmlFreeDoc(ctx);
		} else if (link)
			{}
		else if (!sstrncmp(mime, "text/html") ||
			 !sstrncmp(mime, "application/xhtml+xml"))
			do_html(b, nick, target, url, curl_ctx.data, curl_ctx.len);
		else
			privmsg(b, target, "unhandled content type %s", mime);
	} else if (!link)
		privmsg(b, target, "Error %s (%u)", error, success);

	free(curl_ctx.data);
	curl_easy_cleanup(h);
	return retval;
}

static void command_follow(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	char *space, *last_link = NULL;
	TDB_DATA key, val;
	int ret;
	if (!feed_db)
		return;
	if (target[0] != '#') {
		privmsg(b, target, "%s: Can only follow on channels", nick);
		return;
	}
	if (!args || !*args) {
		privmsg(b, target, "%s: Usage: !follow <url>", nick);
		return;
	}

	if (((space = strchr(args, ' ')) && space < strchr(args, '#')) ||
	    (sstrncmp(args, "http://") && sstrncmp(args, "https://"))) {
		privmsg(b, target, "%s: Invalid url", nick);
		return;
	}

	key.dsize = asprintf((char**)&key.dptr, "%s,%s", target, args)+1;
	val = tdb_fetch(feed_db, key);
	if (val.dptr)
		last_link = strchr(val.dptr, '\001');
	ret = check_single_feed(b, target, key, NULL, args, last_link ? last_link+1 : NULL, nick);
	free(val.dptr);
	if (!ret)
		privmsg(b, target, "%s: Not updated", nick);
	free(key.dptr);
}

static void channel_feed_check(struct bio *b, const char *target, int64_t now)
{
	int len = strlen(target);
	int64_t save[] = { now, 0, 0 };

	TDB_DATA chan, res;
	if (!feed_db || !chan_db)
		return;
	chan.dptr = (char*)target;
	chan.dsize = len+1;
	res = tdb_fetch(chan_db, chan);
	if (res.dptr && res.dsize >= 8) {
		uint64_t then = *(uint64_t*)res.dptr;
		if (now - then <= 8000)
			return;
		if (res.dsize >= 16)
			save[1] = ((uint64_t*)res.dptr)[1];
		if (res.dsize >= 24)
			save[2] = ((uint64_t*)res.dptr)[2];
	}
	free(res.dptr);
	res.dptr = (unsigned char*)save;
	res.dsize = sizeof(save);
	if (tdb_store(chan_db, chan, res, 0) < 0) {
		static int complain_db;
		if (!complain_db++)
			privmsg(b, target, "updating database: %s", tdb_errorstr(feed_db));
		return;
	}

	for (TDB_DATA d = tdb_firstkey(feed_db); d.dptr;) {
		TDB_DATA f = tdb_fetch(feed_db, d);
		TDB_DATA next = tdb_nextkey(feed_db, d);

		if (!strncmp(d.dptr, target, len) && d.dptr[len] == ',') {
			const char *url = (char*)d.dptr + len + 1;
			char *sep;
			if ((sep = strchr(f.dptr, '\001'))) {
				*(sep++) = 0;
				check_single_feed(b, target, d, f.dptr, url, sep, target);
			}
		}
		free(d.dptr);
		free(f.dptr);
		d = next;
	}
}

static void command_unfollow(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	TDB_DATA key;
	char *url;

	if (!feed_db)
		return;

	if (!(url = token(&args, ' ')) || (sstrncmp(url, "http://") && sstrncmp(url, "https://"))) {
		privmsg(b, target, "%s: Invalid url", nick);
		return;
	}
	if (target[0] != '#') {
		privmsg(b, target, "%s: Can only unfollow on channels", nick);
		return;
	}
	key.dsize = asprintf((char**)&key.dptr, "%s,%s", target, url)+1;
	if (tdb_delete(feed_db, key) < 0) {
		if (tdb_error(feed_db) == TDB_ERR_NOEXIST)
			privmsg(b, target, "%s: Not following %s on this channel", nick, url);
		else
			privmsg(b, target, "%s: Could not delete: %s", nick, tdb_errorstr(feed_db));
	} else
		privmsg(b, target, "%s: No longer following %s", nick, url);
	free(key.dptr);
}

static void command_g(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	int ret = 0;
	int64_t g = 0, g_total = 0;
	TDB_DATA chan, res;

	if (!chan_db || target[0] != '#')
		return;

	chan.dptr = (char*)target;
	chan.dsize = strlen(chan.dptr)+1;
	res = tdb_fetch(chan_db, chan);
	if (res.dptr && res.dsize >= 16) {
		g = ((int64_t*)res.dptr)[1];
		((int64_t*)res.dptr)[1] = 0;
		if (res.dsize >= 24)
			g_total = ((int64_t*)res.dptr)[2];
		ret = tdb_store(chan_db, chan, res, 0);
	}
	free(res.dptr);
	if (ret < 0)
		fprintf(stderr, "updating database: %s", tdb_errorstr(feed_db));
	else
		privmsg(b, target, "%s: %"PRIi64" g's since last check, %"PRIi64" total", nick, g, g_total);
}

static void command_feeds(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	int len = strlen(target), found = 0;
	if (!feed_db)
		return;
	if (target[0] != '#') {
		privmsg(b, target, "%s: Only useful in channels..", nick);
		return;
	}
	for (TDB_DATA d = tdb_firstkey(feed_db); d.dptr;) {
		TDB_DATA f = tdb_fetch(feed_db, d);
		TDB_DATA next = tdb_nextkey(feed_db, d);

		if (!strncmp(d.dptr, target, len) && d.dptr[len] == ',') {
			privmsg(b, target, "%s: following %s", nick, d.dptr + len + 1);
			found++;
		}
		free(d.dptr);
		free(f.dptr);
		d = next;
	}
	if (!found)
		privmsg(b, target, "%s: not following any feed on %s", nick, target);
}

static void command_feed_get(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	if (!feed_db)
		return;
	for (TDB_DATA d = tdb_firstkey(feed_db); d.dptr;) {
		TDB_DATA next = tdb_nextkey(feed_db, d);
		if (!args || strcasestr(d.dptr, args)) {
			TDB_DATA f = tdb_fetch(feed_db, d);
			privmsg(b, target, "%s: %s = %s", nick, d.dptr, f.dptr);
			free(f.dptr);
		}
		if (strlen(d.dptr)+1 < d.dsize) {
			privmsg(b, target, "%s: removed buggy entry", nick);
			tdb_delete(feed_db, d);
		}
		free(d.dptr);
		d = next;
	}
}

static void command_feed_set(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	if (!feed_db)
		return;
	TDB_DATA key, val;
	key.dptr = token(&args, ' ');
	char *value = token(&args, ' ');
	if (!key.dptr || !value)
		return;
	key.dsize = strlen(key.dptr) + 1;
	val.dsize = strlen(value) + 2;
	val.dptr = malloc(val.dsize);
	strcpy(val.dptr+1, value);
	val.dptr[0] = '\001';
	if (tdb_store(feed_db, key, val, 0) < 0)
		privmsg(b, target, "%s: setting failed: %s", nick, tdb_errorstr(feed_db));
	else
		privmsg(b, target, "%s: burp", nick);
	free(val.dptr);
}

static void command_feed_rem(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	if (!feed_db || !args)
		return;
	TDB_DATA key = { .dptr = (unsigned char*)args, .dsize = strlen(args)+1 };
	if (tdb_delete(feed_db, key) < 0)
		privmsg(b, target, "%s: removing failed: %s", nick, tdb_errorstr(feed_db));
	else
		privmsg(b, target, "%s: burp", nick);
}

static void command_feed_xxx(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	if (!feed_db)
		return;

	tdb_wipe_all(feed_db);
	privmsg(b, target, "%s: all evidence erased", nick);
}

static void command_seen_xxx(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	if (!seen_db)
		return;

	tdb_wipe_all(seen_db);
	privmsg(b, target, "%s: all evidence erased", nick);
}

static void command_feed_counter(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	if (!chan_db)
		return;
	tdb_wipe_all(chan_db);
	privmsg(b, target, "%s: All update counters reset", nick);
}

static char *get_text_appended(xmlNode *cur)
{
	for (; cur; cur = cur->next) {
		if (cur->type != XML_TEXT_NODE)
			continue;
		return strdup(cur->content);
	}
	return NULL;
}

static char *get_title(struct bio *b, xmlNode *cur_node)
{
	for (; cur_node; cur_node = next_link(cur_node)) {
		if (cur_node->type == XML_ELEMENT_NODE && !strcasecmp(cur_node->name, "title")) {
			if (!cur_node->children)
				return NULL;
			return get_text_appended(cur_node->children);
		}
	}
	return NULL;
}

static void internal_link(struct bio *b, const char *nick, const char *host, const char *target, char *args, unsigned verbose)
{
	CURL *h;
	struct curl_slist *headers = NULL;
	char error[CURL_ERROR_SIZE];
	int success, sent = verbose;
	struct curl_download_context curl_ctx = {};

	if (!args)
		return;
	int64_t stop, start = get_time(b, target);

	h = curl_easy_init();
	headers = curl_slist_append(headers, "User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:9.0.1) Gecko/20100101 Firefox/9.0.1");
	headers = curl_slist_append(headers, "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	headers = curl_slist_append(headers, "Accept-Language: en-us,en;q=0.7");
	headers = curl_slist_append(headers, "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7");
	headers = curl_slist_append(headers, "DNT: 1");
	headers = curl_slist_append(headers, "Connection: keep-alive");
	curl_easy_setopt(h, CURLOPT_HTTPHEADER, headers);
	curl_easy_setopt(h, CURLOPT_URL, args);
	curl_easy_setopt(h, CURLOPT_WRITEFUNCTION, write_data); 
	curl_easy_setopt(h, CURLOPT_WRITEDATA, &curl_ctx); 
	curl_easy_setopt(h, CURLOPT_ERRORBUFFER, error);
	curl_easy_setopt(h, CURLOPT_FOLLOWLOCATION, 1);
	curl_easy_setopt(h, CURLOPT_MAXREDIRS, 3);
	curl_easy_setopt(h, CURLOPT_SSL_VERIFYPEER, 0);
	curl_easy_setopt(h, CURLOPT_TIMEOUT, 8);
	curl_easy_setopt(h, CURLOPT_CONNECTTIMEOUT, 8);
	//curl_easy_setopt(h, CURLOPT_VERBOSE, 1);
	success = curl_easy_perform(h);
	curl_easy_cleanup(h);
	curl_slist_free_all(headers);
	if (success == CURLE_OK) {
		magic_t m = magic_open(MAGIC_MIME_TYPE);
		magic_load(m, NULL);
		const char *mime = magic_buffer(m, curl_ctx.data, curl_ctx.len);
		if (strstr(mime, "text/html") || strstr(mime, "application/xml") || strstr(mime, "application/xhtml+xml")) {
			htmlDocPtr ctx = htmlReadMemory(curl_ctx.data, curl_ctx.len, 0, args, HTML_PARSE_RECOVER|HTML_PARSE_NOERROR|HTML_PARSE_NOWARNING);
			xmlNode *root_element = xmlDocGetRootElement(ctx);
			char *title = get_title(b, root_element);
			if (title) {
				char *nuke;
				squash(title);
				decode_html_entities_utf8(title, NULL);
				if ((nuke = strstr(title, " on SoundCloud - Create")))
					*nuke = 0;
				if (*title) {
					privmsg(b, target, "%s linked %s", nick, title);
					sent = 1;
				}
				free(title);
			}
			if (verbose && !title)
				privmsg(b, target, "%s linked %s page with invalid title", nick, mime);
			xmlFreeDoc(ctx);
		} else if (verbose) {
			magic_setflags(m, MAGIC_COMPRESS);
			const char *desc = magic_buffer(m, curl_ctx.data, curl_ctx.len);
			privmsg(b, target, "%s linked type %s", nick, desc);
		}
		magic_close(m);
	}
	if (verbose && success != CURLE_OK)
		privmsg(b, target, "Error %s (%u)", error, success);
	else if (!sent && (stop = get_time(b, target)) - start >= 15) {
		static uint64_t last_timeout;

		if (stop - last_timeout < 45) {
			privmsg(b, target, "Link (%s) by %s timed out, disabling links for 10 seconds", args, nick);
			commands[strhash("get") % elements(commands)].disabled_until = stop + 10;
		}
		last_timeout = stop;
	}
	free(curl_ctx.data);
}

static void command_get(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	if (!args || (sstrncmp(args, "http://") && sstrncmp(args, "https://")))
		return;
	internal_link(b, nick, host, target, token(&args, ' '), 1);
}

static void command_fabric(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	privmsg(b, target, "Dumb fabric!");
}

static void command_fun(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	privmsg(b, target, "Fun?");
}

static void command_admire(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	char *arg = token(&args, ' ');
	privmsg(b, target, "%s: I really like your mane!", arg ? arg : nick);
}

static void command_hugs(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	action(b, target, "gives a lunar hug to %s", args ? args : nick);
}

static void command_hug(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	if ((host && !strcmp(host, "life.on.the.moon.is.great.I.know.all.about.it")) ||
	    (args && !sstrncasecmp(args, "lu")))
		command_hugs(b, nick, host, target, args);
	else
		action(b, target, "gives a robotic hug to %s", args ? args : nick);
}

static void command_snuggles(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	action(b, target, "gives a lunar snuggle to %s", args ? args : nick);
}

static void command_snuggle(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	if ((host && !strcmp(host, "life.on.the.moon.is.great.I.know.all.about.it")) ||
	    (args && !sstrncasecmp(args, "lu")))
		command_snuggles(b, nick, host, target, args);
	else
		action(b, target, "gives a robotic snuggle to %s", args ? args : nick);
}

static void command_cuddles(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	action(b, target, "gives cuddles to %s in a lunaresque way", args ? args : nick);
}

static void command_cuddle(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	if ((host && !strcmp(host, "life.on.the.moon.is.great.I.know.all.about.it")) ||
	    (args && !sstrncasecmp(args, "lu")))
		command_cuddles(b, nick, host, target, args);
	else
		action(b, target, "gives cuddles to %s in a robotic way", args ? args : nick);
}


static void command_cookie(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	if (args && !strncasecmp(args, nick_self, strlen(nick_self)))
		action(b, target, "eats the cookie offered by %s", nick);
	else
		action(b, target, "hands a metallic looking cookie to %s", args ? args : nick);
}

static void command_derpy(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	static const char *msgs[] = {
		"{ SweetieBelle sad! Beep, boop.. beep. }",
	};
        static const char *insults[] = {
		"accidentally shocks herself",
		"tumbles down the stairs like a slinky",
		"whirrrs and clicks in a screeching way",
		"had problems executing this command",
		"breaks down entirely",
		"uses her magic to levitate herself off the ground, then hits it face first"
	};
	int n = getrand() % (elements(msgs) + elements(insults));
	if (n < elements(msgs))
		privmsg(b, target, "%s", msgs[n]);
	else
		action(b, target, "%s", insults[n - elements(msgs)]);
}

static void command_inspect(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	struct command_hash *c;
	unsigned leave, crash;
	char *cmd;
	if (!args || !(cmd = token(&args, ' ')))
		return;

	if (strcmp(cmd, "#")) {
		c = &commands[strhash(cmd) % elements(commands)];
		if (!c->string || strcasecmp(c->string, cmd)) {
			privmsg(b, target, "Command %s not valid", cmd);
			return;
		}
	} else
		c = &command_channel;

	leave = c->left + (c->cmd == command_inspect);
	crash = c->enter - leave;
	if (c->enter != leave)
		privmsg(b, target, "%s: %u successes and %u crash%s, last crashing command: %s", c->string, leave, crash, crash == 1 ? "" : "es", c->failed_command);
	else
		privmsg(b, target, "%s: %u time%s executed succesfully", c->string, leave, leave == 1 ? "" : "s");
}

static void command_rebuild(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	int ret;
	char *make[] = { "/usr/bin/make", "-j4", NULL };
	char *git_reset[] = { "/usr/bin/git", "reset", "--hard", "master", NULL };
	ret = pipe_command(b, target, nick, 0, 1, git_reset);
	if (ret) {
		action(b, target, "could not rebuild");
		return;
	}
	ret = pipe_command(b, target, nick, 0, 1, make);
	if (!ret)
		kill(getpid(), SIGUSR1);
	else if (ret > 0)
		action(b, target, "displays an ominous %i", ret);
}

static void command_swear(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	static const char *insults[] = {
		"featherbrain",
		"ponyfeathers",
		"What in the hey?",
		"What are you, a dictionary?",
		"TAR-DY!",
		"[BUY SOME APPLES]",
		"{ Your lack of bloodlust on the battlefield is proof positive that you are a soulless automaton! }",
		"Your royal snootiness"
	};
	privmsg(b, target, "%s: %s", args ? args : nick, insults[getrand() % elements(insults)]);
}

static const char *perty(int64_t *t)
{
	if (*t >= 14 * 24 * 3600) {
		*t /= 7 * 24 * 3600;
		return "weeks";
	} else if (*t >= 48 * 3600) {
		*t /= 24 * 3600;
		return "days";
	} else if (*t >= 7200) {
		*t /= 3600;
		return "hours";
	} else if (*t >= 120) {
		*t /= 60;
		return "minutes";
	}
	return *t == 1 ? "second" : "seconds";
}

static void command_timeout(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	struct command_hash *c;
	int64_t t = get_time(b, target);
	int64_t howlong;
	if (t < 0)
		return;
	char *arg = token(&args, ' ');
	if (!arg || !args || !(howlong = atoi(args))) {
		action(b, target, "pretends to time out");;
		return;
	}
	c = &commands[strhash(arg) % elements(commands)];
	if (c->string && !strcasecmp(c->string, arg)) {
		c->disabled_until = t + howlong;
		const char *str = perty(&howlong);
		action(b, target, "disables %s for %"PRIi64" %s", arg, howlong, str);
	} else
		action(b, target, "clicks sadly at %s for not being able to find that command", nick);
}

static void command_mfw(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	char error[CURL_ERROR_SIZE], *new_url;
	CURL *h = curl_easy_init();
	struct curl_slist *headers = NULL;
	struct curl_download_context curl_ctx = {};
	headers = curl_slist_append(headers, "User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:9.0.1) Gecko/20100101 Firefox/9.0.1");
	headers = curl_slist_append(headers, "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
	headers = curl_slist_append(headers, "Accept-Language: en-us,en;q=0.7");
	headers = curl_slist_append(headers, "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7");
	headers = curl_slist_append(headers, "DNT: 1");
	headers = curl_slist_append(headers, "Connection: keep-alive");
	curl_easy_setopt(h, CURLOPT_HTTPHEADER, headers);
	curl_easy_setopt(h, CURLOPT_URL, "http://mylittlefacewhen.com/random/");
	curl_easy_setopt(h, CURLOPT_WRITEFUNCTION, write_data); 
	curl_easy_setopt(h, CURLOPT_WRITEDATA, &curl_ctx); 
	curl_easy_setopt(h, CURLOPT_ERRORBUFFER, error);
	curl_easy_setopt(h, CURLOPT_TIMEOUT, 8);
	curl_easy_setopt(h, CURLOPT_CONNECTTIMEOUT, 8);
	//curl_easy_setopt(h, CURLOPT_VERBOSE, 1);
	CURLcode ret = curl_easy_perform(h);
	if (ret == CURLE_OK && curl_easy_getinfo(h, CURLINFO_REDIRECT_URL, &new_url) == CURLE_OK)
		privmsg(b, target, "%s: %s", nick, new_url);
	curl_slist_free_all(headers);
	curl_easy_cleanup(h);
	if (ret != CURLE_OK)
		privmsg(b, target, "%s: You have no face", nick);
}

static TDB_DATA get_mail_key(const char *nick)
{
	TDB_DATA d;
	int i;
	d.dsize = strlen(nick)+1;
	d.dptr = malloc(d.dsize);
	for (i = 0; i < d.dsize - 1; ++i)
		d.dptr[i] = tolower(nick[i]);
	d.dptr[i] = 0;
	return d;
}

static void command_mail(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	const char *ident = &nick[strlen(nick) + 1]; // HACK
	char *victim, *x;
	size_t len;
	int override = 0;
	int64_t last_seen = 0;

	if (!mail_db || !seen_db)
		return;
	TDB_DATA key, val;
	victim = token(&args, ' ');
	if (victim && !strcasecmp(victim, "-seen")) {
		victim = token(&args, ' ');
		override = 1;
	}
	x = victim;
	victim = token(&x, ',');
	if (!victim || x || !args || victim[0] == '#' || strchr(victim, '@') || strchr(victim, '.')) {
		privmsg(b, target, "%s: Usage: !mail <nick> <message>", nick);
		return;
	}
	if (!strcasecmp(victim, nick_self)) {
		action(b, target, "whirrs and clicks excitedly at the mail she received from %s", nick);
		return;
	}
	if (!strcasecmp(victim, nick)) {
		action(b, target, "echos the words from %s back to them: %s", nick, args);
		return;
	}
	int64_t now = get_time(b, target);
	if (now < 0)
		return;

	key = get_mail_key(victim);
	val = tdb_fetch(seen_db, key);
	if (val.dptr && (x = strchr(val.dptr, ','))) {
		*(x++) = 0;
		last_seen = atoll(val.dptr);
	}
	if (x && !admin(ident, host) && (now - last_seen) < 300 && (!sstrncasecmp(x, "in ") || !sstrncasecmp(x, "joining "))) {
		action(b, target, "would rather not store mail for someone active so recently");
			goto out;
	} else if (last_seen && now - last_seen > 14 * 24 * 3600 && !override) {
		int64_t delta = now - last_seen;
		const char *str = perty(&delta);
		privmsg(b, target, "%s: \"%s\" was last seen %"PRIi64" %s ago, use !mail -seen %s <message> to override this check.", nick, victim, delta, str, victim);
		return;
	} else if (!x && !override) {
		privmsg(b, target, "%s: I've never seen \"%s\", use !mail -seen %s <message> to override this check.", nick, victim, victim);
		return;
	}

	val = tdb_fetch(mail_db, key);
	if (!val.dptr)
		val.dsize = 0;
	else {
		unsigned char *cur;
		int letters = 0;
		for (cur = val.dptr; cur < val.dptr + val.dsize; cur += strlen(cur)+1)
			letters++;
		if (letters >= 6) {
			action(b, target, "looks sadly at %s as she cannot hold any more mail to %s", nick, victim);
			goto out;
		}
	}
	len = snprintf(NULL, 0, "%"PRIi64 ",%s: %s", now, nick, args) + 1;
	val.dptr = realloc(val.dptr, val.dsize + len);
	snprintf(val.dptr + val.dsize, len, "%"PRIi64 ",%s: %s", now, nick, args);
	val.dsize += len;
	if (tdb_store(mail_db, key, val, 0) < 0)
		privmsg(b, target, "%s: updating mail returns %s", nick, tdb_errorstr(mail_db));
	else
		action(b, target, "whirrs and clicks at %s as she stores the mail for %s", nick, victim);
out:
	free(val.dptr);
	free(key.dptr);
}

static void single_message(struct bio *b, const char *target, char *cur, int64_t now)
{
	char *endtime, *sep = strchr(cur, ':');
	int64_t delta = -1;
	if (sep && (endtime = strchr(cur, ',')) && endtime < sep) {
		int64_t t = atoll(cur);
		if (t > 0)
			delta = now - t;
		cur = endtime + 1;
	}
	if (delta >= 0) {
		const char *str = perty(&delta);
		privmsg(b, target, "%"PRIi64" %s ago from %s", delta, str, cur);
	} else
		privmsg(b, target, "From %s", cur);
}

static void command_deliver(struct bio *b, const char *nick, const char *target)
{
	TDB_DATA key, val;
	unsigned char *cur;
	static unsigned mail_enter, mail_leave;
	if (mail_enter++ != mail_leave)
		return;

	if (!mail_db)
		return;
	key = get_mail_key(nick);
	val = tdb_fetch(mail_db, key);
	if (!val.dptr)
		goto end;
	int64_t now = get_time(b, NULL);
	if (strcasecmp(key.dptr, nick_self)) {
		privmsg(b, target, "%s: You've got mail!", nick);
		for (cur = val.dptr; cur < val.dptr + val.dsize; cur += strlen(cur)+1)
			single_message(b, target, cur, now);
	}
	free(val.dptr);
	tdb_delete(mail_db, key);
end:
	free(key.dptr);
	mail_leave++;
}

static void update_seen(struct bio *b, char *doingwhat, const char *nick)
{
	TDB_DATA key;
	key = get_mail_key(nick);
	TDB_DATA val = { .dptr = doingwhat, .dsize = strlen(doingwhat)+1 };
	if (seen_db)
		tdb_store(seen_db, key, val, 0);
	free(key.dptr);
}

static void command_seen(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	char *arg = token(&args, ' '), *x;
	int64_t now = get_time(b, target);
	TDB_DATA key, val;
	if (now < 0)
		return;
	if (!seen_db || !arg) {
		privmsg(b, target, "%s: { Error... }", nick);
		return;
	}
	if (!strcasecmp(arg, nick_self)) {
		action(b, target, "whirrs and clicks at %s", nick);
		return;
	}
	if (!strcasecmp(arg, nick)) {
		action(b, target, "circles around %s dancing", nick);
		return;
	}
	key = get_mail_key(arg);
	val = tdb_fetch(seen_db, key);
	if (val.dptr && (x = strchr(val.dptr, ','))) {
		int64_t delta;
		const char *str;
		*(x++) = 0;
		delta = now - atoll(val.dptr);
		str = perty(&delta);
		if (delta < 0)
			privmsg(b, target, "%s was last seen in the future %s", arg, x);
		else
			privmsg(b, target, "%s was last seen %"PRIi64" %s ago %s", arg, delta, str, x);
	} else
		action(b, target, "cannot find any evidence that %s exists", arg);
	free(val.dptr);
}

static int suppress_message(const char *cur, int64_t now)
{
	const char *endtime, *sep = strchr(cur, ':');
	int64_t delta = -1;
	if (sep && (endtime = strchr(cur, ',')) && endtime < sep) {
		int64_t t = atoll(cur);
		if (t > 0)
			delta = now - t;
	}

	return delta == -1 || delta >= 28 * 24 * 3600;
}

static void command_mailbag(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	char buffer[256], *trunc = NULL;
	unsigned rem = sizeof(buffer)-1, first = 2, hidden = 0;
	int64_t now = get_time(b, NULL);
	int show_recent = 1, show_old = 0;
	char *show;

	if (args && (show = token(&args, ' '))) {
		if (!strcasecmp(show, "all"))
			show_old = 1;
		else if (!strcasecmp(show, "old")) {
			show_old = 1;
			show_recent = 0;
		}
	}

	if (!mail_db)
		return;
	buffer[rem] = 0;

	for (TDB_DATA f = tdb_firstkey(mail_db); f.dptr;) {
		TDB_DATA next;

		if (f.dsize + 4 > rem) {
			privmsg(b, target, "%s: Holding mail for: %s", nick, &buffer[rem]);
			first = 2;
			rem = sizeof(buffer)-1;
			trunc = NULL;
			assert(f.dsize + 4 < rem);
		}
		if (f.dptr) {
			TDB_DATA val = tdb_fetch(mail_db, f);
			if (val.dptr) {
				int hide = now >= 0 && suppress_message(val.dptr, now);
				if ((!show_recent && !hide) || (!show_old && hide)) {
					hidden++;
					free(val.dptr);
					goto skip;
				}
				free(val.dptr);
			}

			if (first == 2) {
				first = 1;
			} else if (first) {
				rem -= 5;
				memcpy(&buffer[rem], " and ", 5);
				first = 0;
				trunc = &buffer[rem];
			} else {
				rem -= 2;
				memcpy(&buffer[rem], ", ", 2);
			}
			rem -= f.dsize - 1;
			memcpy(&buffer[rem], f.dptr, f.dsize - 1);
		}
skip:
		next = tdb_nextkey(mail_db, f);
		free(f.dptr);
		f = next;
	}
	if (first < 2 && !hidden)
		privmsg(b, target, "%s: Holding mail for: %s", nick, &buffer[rem]);
	else if (trunc) {
		*trunc = 0;
		trunc += 5;
		privmsg(b, target, "%s: Holding mail for: %s, %s and %u others", nick, &buffer[rem], trunc, hidden);
	} else
		privmsg(b, target, "%s: Holding mail for: %s and %u others", nick, &buffer[rem], hidden);
}

static void command_mailread(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	TDB_DATA key, val;
	char *victim;
	if (!mail_db || !(victim = token(&args, ' ')))
		return;
	key = get_mail_key(victim);
	val = tdb_fetch(mail_db, key);
	if (!val.dptr)
		action(b, target, "ponyshrugs as no mail for %s was found", victim);
	else {
		unsigned char *cur;
		int64_t now = get_time(b, NULL);
		action(b, target, "peeks through %s's mail", victim);
		for (cur = val.dptr; cur < val.dptr + val.dsize; cur += strlen(cur)+1)
			single_message(b, target, cur, now);
	}
	free(val.dptr);
	free(key.dptr);
}

static void command_no_deliver(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	TDB_DATA key, val;
	char *cur;
	if (!mail_db || !(cur = token(&args, ' ')))
		return;
	key = get_mail_key(cur);
	val = tdb_fetch(mail_db, key);
	if (!val.dptr)
		action(b, target, "ponyshrugs as no mail for %s was found", cur);
	else {
		action(b, target, "deletes all evidence of %s's mail", cur);
		tdb_delete(mail_db, key);
	}
	free(val.dptr);
	free(key.dptr);
}

static void perform_roll(struct bio *b, const char *nick, const char *target, long sides, long dice, long bonus)
{
	long rem = dice, total = bonus;

	if (!sides) {
		// Fate dice..
		while (rem--)
			total += (getrand() % 3) - 1;

		if (bonus)
			action(b, target, "rolls %li fate %s for a total of %li (%+li)", dice, dice == 1 ? "die" : "dice", total, bonus);
		else
			action(b, target, "rolls %li fate %s for a total of %li", dice, dice == 1 ? "die" : "dice", total);
		return;
	}

	while (rem--)
		total += 1 + (getrand() % sides);

	if (bonus)
		action(b, target, "rolls %li %li-sided %s for a total of %li (%+li)", dice, sides, dice == 1 ? "die" : "dice", total, bonus);
	else
		action(b, target, "rolls %li %li-sided %s for a total of %li", dice, sides, dice == 1 ? "die" : "dice", total);
}

static void command_roll(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	char *cur;
	long dice = 1, sides = 20;
	if ((cur = token(&args, ' '))) {
		char *first = cur;
		dice = strtol(first, &cur, 10);
		if (first == cur)
			dice = 1;
		if (*cur && *cur != 'd' && *cur != 'D')
			goto syntax;

		if (*cur) {
			char *last = cur+1;
			sides = strtol(last, &cur, 10);
			if (last == cur)
				goto syntax;
		}
	}
	if (dice <= 0 || dice > 25 || sides < 2 || sides > 20)
		goto syntax;
	perform_roll(b, nick, target, sides, dice, 0);
	return;

syntax:
	action(b, target, "bleeps at %s in a confused manner", nick);
}

static void add_g(struct bio *b, const char *target, int g)
{
	int ret = 0;
	int64_t total_g = 0;
	TDB_DATA chan, res;

	if (!chan_db || target[0] != '#')
		return;

	chan.dptr = (char*)target;
	chan.dsize = strlen(chan.dptr)+1;
	res = tdb_fetch(chan_db, chan);
	if (res.dptr && res.dsize >= 16) {
		((int64_t*)res.dptr)[1] += g;
		if (res.dsize >= 24)
			total_g = ((int64_t*)res.dptr)[2] += g;
		ret = tdb_store(chan_db, chan, res, 0);
	}
	free(res.dptr);
	if (ret < 0)
		fprintf(stderr, "updating database: %s", tdb_errorstr(feed_db));
	else if (g < total_g) {
		int64_t last_g = total_g - g;
		last_g -= last_g % 1000;
		if (last_g + 1000 <= total_g)
			privmsg(b, target, "g has been said %"PRIi64" times!", total_g);
	}
}

static int parse_g(const char *cur, int *g)
{
	int this_g = 0;
	for (const unsigned char *ptr = cur; *ptr; ptr++) {
		if (*ptr >= 0x80)
			return 0;
		if (*ptr == 'g' || *ptr == 'G')
			this_g++;
		else if ((*ptr >= 'a' && *ptr <= 'z') || (*ptr >= 'A' && *ptr <= 'Z'))
			return 0;
		else if (*ptr != '1' && *ptr >= '0' && *ptr <= '9')
			return 0;
	}
	*g += this_g;
	return this_g;
}

static void channel_msg(struct bio *b, const char *nick, const char *host,
                        const char *chan, char *msg, int64_t t)
{
	char *cur = NULL, *next;
	int is_action = 0;
	int g = 0;

	int that = 0, what = 0, she = 0, nsfw = 0;

	if (!msg || !strcmp(nick, "`Daring_Do`") ||
	    !sstrncmp(nick, "derpy") || !sstrncmp(nick, "`derpy") ||
	    !sstrncmp(nick, "`Luna") || !sstrncmp(nick, "`Celestia") ||
	    !sstrncmp(nick, "GitHub") || !sstrncmp(nick, "CIA-") ||
	    !sstrncmp(nick, "Termi") || !strcmp(nick, "r0m") ||
	    !sstrncasecmp(nick, "Owloysius") || strstr(host, "/bot/"))
		return;

	if (!sstrncasecmp(msg, "\001ACTION ")) {
		msg += sizeof("\001ACTION ")-1;
		is_action = 1;
		asprintf(&cur, "%"PRIi64 ",in %s: * %s %s", t, chan, nick, msg);
	} else
		asprintf(&cur, "%"PRIi64 ",in %s: <%s> %s", t, chan, nick, msg);
	if (t > 0 && strcasecmp(chan, "#themoon") && strcasecmp(chan, "#fluttertreehouse"))
		update_seen(b, cur, nick);
	free(cur);
	(void)is_action;

	if (!sstrncasecmp(msg, "fun!") || !strcasecmp(msg, "fun")) {
		static int64_t last_fun = -1;

		if (!strcasecmp(chan, "#bronymusic") && t - last_fun > 5) {
			if (t < 0 || t > commands[strhash("fun") % elements(commands)].disabled_until) {
				if (sstrncmp(nick, "EqBot"))
					privmsg(b, chan, "Fun!");
				privmsg(b, chan, "Fun!");
			}
		}
		last_fun = t;
		return;
	}

	next = msg;
	while ((cur = token(&next, ' '))) {
		if (!strcasecmp(cur, ">mfw") || !strcasecmp(cur, "mfw")) {
			if (!strcasecmp(chan, "#brony") || !ponify)
				continue;
			if (t < 0 || t > commands[strhash("mfw") % elements(commands)].disabled_until)
				command_mfw(b, nick, host, chan, NULL);
			return;
		} else if (!sstrncasecmp(cur, "http://") || !sstrncasecmp(cur, "https://")) {
			static char last_url[512];
			char *part;
			if (!strcmp(cur, last_url))
				return;
			strncpy(last_url, cur, sizeof(last_url)-1);

			if (!sstrncmp(nick, "EqBot") || !sstrncasecmp(chan, "#mlpsurvival"))
				return;

			if (t >= 0 && t < commands[strhash("get") % elements(commands)].disabled_until)
				return;

			else if (strcasestr(cur, "youtube.com/user") && (part = strstr(cur, "#p/"))) {
				char *foo;
				part = strrchr(part, '/') + 1;
				asprintf(&foo, "http://youtube.com/watch?v=%s", part);
				if (foo)
					internal_link(b, nick, host, chan, foo, 0);
				free(foo);
				return;
			} else if (strcasestr(cur, "twitter.com/") || strcasestr(cur, "irccloud.com/pastebin/") || strcasestr(cur, "mlfw.info") || strcasestr(cur, "mylittlefacewhen.com") || !strcasecmp(chan, "#geek"))
				return;
			if (!strcasecmp(chan, "#bronymusic") &&
				(nsfw || (next && strcasestr(next, "nsfw"))))
				continue;
			internal_link(b, nick, host, chan, cur, 0);
			return;
		}
		else if (!sstrncasecmp(cur, "that") || !sstrncasecmp(cur, "dat"))
			that = 1;
		else if (that && (!sstrncasecmp(cur, "what") || !sstrncasecmp(cur, "wat")))
			what = 1;
		else if (what && !sstrncasecmp(cur, "she"))
			she = 1;
		else if (she && !sstrncasecmp(cur, "said")) {
			if (t <= 0 || t >= commands[strhash("shesaid") % elements(commands)].disabled_until)
				privmsg(b, chan, "%s: \"%s\"", nick, women_quotes[getrand() % elements(women_quotes)]);
			return;
		} else if (parse_g(cur, &g))
			continue;
		else if (strcasestr(cur, "nsfw"))
			nsfw = 1;
	}
	if (g)
		add_g(b, chan, g);
}

static void command_commands(struct bio *b, const char *nick, const char *host, const char *target, char *args);

static struct command_hash unhashed[] = {
	{ "1", command_coinflip },
	{ "fabric", command_fabric },
	{ "fun", command_fun },
	{ "get", command_get },
	{ "hug", command_hug },
	{ "hugs", command_hugs, -1 },
	{ "admire", command_admire },
	{ "snug", command_snuggle, -1 },
	{ "snugs", command_snuggles, -1 },
	{ "snuggle", command_snuggle },
	{ "snuggles", command_snuggles, -1 },
	{ "cuddle", command_cuddle },
	{ "cuddles", command_cuddles, -1 },
	{ "cookie", command_cookie },
	{ "mfw", command_mfw },
	{ "swear", command_swear },
	{ "mail", command_mail },
	{ "m", command_mail, -1 },
	{ "seen", command_seen },
	{ "derpy", command_derpy },
	{ "g", command_g, -1 },
	{ "shesaid", command_shesaid },
	{ "roll", command_roll },

	{ "rebuild", command_rebuild, 1 },
	{ "abort", command_abort, 1 },
	{ "crash", command_crash, 1 },
	{ "inspect", command_inspect, 1 },
	{ "timeout", command_timeout, 1 },

	{ "follow", command_follow },
	{ "unfollow", command_unfollow },
	{ "feeds", command_feeds },

// DEBUG
	{ "feed_get", command_feed_get, 1 },
	{ "feed_set", command_feed_set, 1 },
	{ "feed_rem", command_feed_rem, 1 },
	{ "feed_xxx", command_feed_xxx, 1 },
	{ "feed_counter", command_feed_counter, 1 },
	{ "seen_xxx", command_seen_xxx, 1 },
	{ "mailbag", command_mailbag },
	{ "mailread", command_mailread, 1 },
	{ "\"deliver\"", command_no_deliver, 1 },
	{ "commands", command_commands, -1 }
};

static void command_commands(struct bio *b, const char *nick, const char *host, const char *target, char *args)
{
	char buffer[256];
	unsigned rem = sizeof(buffer)-1, first = 2, i;
	buffer[rem] = 0;

	for (i = 0; i < sizeof(unhashed)/sizeof(*unhashed); ++i) {
		const char *str;
		unsigned long len;
		if (unhashed[i].admin)
			continue;

		str = unhashed[i].string;
		len = strlen(str);

		if (len + 5 > rem) {
			privmsg(b, target, "%s: Valid commands: %s", nick, &buffer[rem]);
			first = 2;
			rem = sizeof(buffer)-1;
			assert(len < rem);
		}
		if (str) {
			if (first == 2) {
				first = 1;
			} else if (first) {
				rem -= 5;
				memcpy(&buffer[rem], " and ", 5);
				first = 0;
			} else {
				rem -= 2;
				memcpy(&buffer[rem], ", ", 2);
			}
			rem -= len;
			memcpy(&buffer[rem], str, len);
		}
	}
	if (first < 2)
		privmsg(b, target, "%s: Valid commands: %s", nick, &buffer[rem]);
}

static void init_hash(struct bio *b, const char *target)
{
	int i;
	for (i = 0; i < elements(unhashed); ++i) {
		unsigned h = strhash(unhashed[i].string) % elements(commands);
		if (commands[h].string)
			privmsg(b, target, "%s is a duplicate command with %s", commands[h].string, unhashed[i].string);
		else
			commands[h] = unhashed[i];
	}
#ifdef local_commands
	for (i = 0; i < elements(local_commands); ++i) {
		unsigned h = strhash(local_commands[i].string) % elements(commands);
		if (commands[h].string)
			privmsg(b, target, "%s is a duplicate command with %s", commands[h].string, local_commands[i].string);
		else
			commands[h] = local_commands[i];
	}
#endif
}

void init_hook(struct bio *b, const char *target, const char *nick, unsigned is_ponified)
{
	char *cwd, *path = NULL;
	nick_self = nick;
	static const char *messages[] = {
		"feels circuits being activated that weren't before",
		"suddenly gets a better feel of her surroundings",
		"looks the same, yet there's definitely something different",
		"emits a beep as her lights begin to pulse slowly",
		"whirrrs and bleeps like never before",
		"bleeps a few times happily",
		"excitedly peeks at her surroundings"
	};
	init_hash(b, target);
	ponify = is_ponified;

	cwd = getcwd(NULL, 0);
	asprintf(&path, "%s/db/feed.tdb", cwd);
	feed_db = tdb_open(path, 0, 0, O_RDWR|O_CREAT|O_CLOEXEC|O_NOCTTY, 0644);
	free(path);
	if (!feed_db)
		privmsg(b, target, "Opening feed db failed: %m");

	asprintf(&path, "%s/db/chan.tdb", cwd);
	chan_db = tdb_open(path, 0, 0, O_RDWR|O_CREAT|O_CLOEXEC|O_NOCTTY, 0644);
	free(path);
	if (!chan_db)
		privmsg(b, target, "Opening chan db failed: %m");

	asprintf(&path, "%s/db/mail.tdb", cwd);
	mail_db = tdb_open(path, 0, 0, O_RDWR|O_CREAT|O_CLOEXEC|O_NOCTTY, 0644);
	free(path);
	if (!mail_db)
		privmsg(b, target, "Opening mail db failed: %m");

	asprintf(&path, "%s/db/seen.tdb", cwd);
	seen_db = tdb_open(path, 0, 0, O_RDWR|O_CREAT|O_CLOEXEC|O_NOCTTY, 0644);
	free(path);
	if (!seen_db)
		privmsg(b, target, "Opening seen db failed: %m");

	free(cwd);
	action(b, target, "%s", messages[getrand() % elements(messages)]);
}

static void  __attribute__((destructor)) shutdown_hook(void)
{
	if (seen_db)
		tdb_close(seen_db);
	if (mail_db)
		tdb_close(mail_db);
	if (feed_db)
		tdb_close(feed_db);
	if (chan_db)
		tdb_close(chan_db);
}

static char *nom_special(char *line)
{
	while (*line) {
		if (*line == 0x03) {
			line++;
			if (*line >= '0' && *line <= '9')
				line++;
			else continue;
			if (*line >= '0' && *line <= '9')
				line++;
			if (line[0] == ',' && line[1] >= '0' && line[1] <= '9')
				line += 2;
			else continue;
			if (*line >= '0' && *line <= '9')
				line++;
		} else if (*line >= 0x02 && *line <= 0x1f)
			/* some IRC control code that's not CTCP */
			line++;
		else
			return line;
	}
	return line;
}

static char *cleanup_special(char *line)
{
	char *cur, *start = nom_special(line);
	if (!*start)
		return NULL;
	for (line = cur = start; *line; line = nom_special(line))
		*(cur++) = *(line++);

	for (cur--; cur >= start; --cur)
		if (*cur != ' ' && *cur != '\001')
			break;

	if (cur < start)
		return NULL;
	cur[1] = 0;
	return start;
}

static void rss_check(struct bio *b, const char *channel, int64_t t)
{
	static unsigned rss_enter, rss_leave;
	if (t >= 0 && rss_enter == rss_leave) {
		rss_enter++;
		channel_feed_check(b, channel, t);
		rss_leave++;
	}
}

static const char *privileged_command[] = {
	"{ Rarity, I love you so much! }",
	"{ Rarity, have I ever told you that I love you? }",
	"{ Yes, I love my sister, Rarity. }",
	"{ Raaaaaaaaaaaaaarity. }",
	"{ You do not fool me, Rari...bot! }"
};

static int cmd_check(struct bio *b, struct command_hash *c, int is_admin, int64_t t, const char *prefix, const char *target)
{
	if (c->left != c->enter)
		privmsg(b, target, "Command %s is disabled because of a crash", c->string);
	else if (t > 0 && t < c->disabled_until && !is_admin) {
		int64_t delta = c->disabled_until - t;
		const char *str = perty(&delta);
		b->writeline(b, "NOTICE %s :Command %s is on timeout for the next %"PRIi64 " %s", prefix, c->string, delta, str);
	} else if (c->admin > 0 && !is_admin)
		privmsg(b, target, "%s: %s", prefix, privileged_command[getrand() % elements(privileged_command)]);
	else
		return 1;
	return 0;
}

void privmsg_hook(struct bio *b, const char *prefix, const char *ident, const char *host,
                  const char *const *args, unsigned nargs)
{
	char *cmd_args, *cmd;
	const char *target = args[0][0] == '#' ? args[0] : prefix;
	unsigned chan, nick_len;
	struct command_hash *c;
	int64_t t = get_time(b, target);
	int is_admin = admin(ident, host);

	chan = args[0][0] == '#';
	if (chan) {
		rss_check(b, args[0], t);
		command_deliver(b, prefix, args[0]);
	}
	cmd_args = cleanup_special((char*)args[1]);
	if (!cmd_args)
		return;

	if (ident && (!strcasecmp(ident, "Revolver") || !strcasecmp(ident, "Rev")))
		return;

	if (chan && cmd_args[0] == '!') {
		cmd_args++;
	} else if (chan && (nick_len = strlen(nick_self)) &&
		!strncasecmp(cmd_args, nick_self, nick_len) &&
		(cmd_args[nick_len] == ':' || cmd_args[nick_len] == ',') && cmd_args[nick_len+1] == ' ') {
		cmd_args += nick_len + 2;
		if (!cmd_args[0])
			return;
		chan = 2;
	} else if (chan) {
		if (command_channel.enter == command_channel.left) {
			command_channel.enter++;
			snprintf(command_channel.failed_command,
				 sizeof(command_channel) - offsetof(struct command_hash, failed_command) - 1,
				 "%s:%s (%s@%s) \"#\" %s", target, prefix, ident, host, cmd_args);
			channel_msg(b, prefix, host, args[0], cmd_args, t);
			command_channel.left++;
		}
		return;
	}
	cmd = token(&cmd_args, ' ');
	if (!chan && cmd_args && cmd_args[0] == '#') {
		if (!is_admin) {
			privmsg(b, target, "%s: %s", prefix, privileged_command[getrand() % elements(privileged_command)]);
			return;
		}
		target = token(&cmd_args, ' ');
	}

	c = &commands[strhash(cmd) % elements(commands)];
	if (c->string && !strcasecmp(c->string, cmd)) {
		if (!sstrncasecmp(prefix, "EqBot")) {
			action(b, target, "adoringly pulses some light back to %s", prefix);
			return;
		}
		if (cmd_check(b, c, is_admin, t, prefix, target)) {
			c->enter++;
			snprintf(c->failed_command, sizeof(*c) - offsetof(struct command_hash, failed_command) - 1,
				 "%s:%s (%s@%s) \"%s\" %s", target, prefix, ident, host, cmd, cmd_args);
			c->cmd(b, prefix, host, target, cmd_args);
			c->left++;
		}
		return;
	} else if (cmd[0] == 'd' && cmd[1] >= '0' && cmd[1] <= '9') {
		char *end;
		long base;
		base = strtol(cmd+1, &end, 10);
		if ((base > 1 || !base) && base <= 20 && !*end) {
			long dice = 1, bonus = 0;
			char *strdice;
			c = &commands[strhash("roll") % elements(commands)];
			if (!cmd_check(b, c, is_admin, t, prefix, target))
				return;
			c->enter++;
			snprintf(c->failed_command, sizeof(*c) - offsetof(struct command_hash, failed_command) - 1,
				"%s:%s (%s@%s) \"%s\" %s", target, prefix, ident, host, cmd, cmd_args);

			strdice = token(&cmd_args, ' ');
			if (strdice) {
				dice = strtol(strdice, &end, 10);
				if (*end || !dice || dice > 25)
					goto syntax;
				strdice = token(&cmd_args, ' ');
				if (strdice) {
					bonus = strtol(strdice, &end, 10);
					if (*end || bonus > 1000 || bonus < -1000)
						goto syntax;
				}
			}
			perform_roll(b, prefix, target, base, dice, bonus);
			c->left++;
			return;

syntax:
			action(b, target, "bleeps at %s in a confused manner", prefix);
			c->left++;
			return;
		}
	} else if (cmd[0] >= '0' && cmd[0] <= '9') {
		long dice, sides, bonus = 0;
		char *end;
		dice = strtol(cmd, &end, 10);
		if (dice >= 1 && dice <= 25 && *end == 'd' &&
		    ((sides = strtol(end+1, &end, 10)) > 1 || !sides) &&
		    sides <= 20 && !*end) {
			char *strbonus;

			c = &commands[strhash("roll") % elements(commands)];
			if (!cmd_check(b, c, is_admin, t, prefix, target))
				return;
			c->enter++;
			snprintf(c->failed_command, sizeof(*c) - offsetof(struct command_hash, failed_command) - 1,
				"%s:%s (%s@%s) \"%s\" %s", target, prefix, ident, host, cmd, cmd_args);

			strbonus = token(&cmd_args, ' ');
			if (strbonus) {
				bonus = strtol(strbonus, &end, 10);
				if (*end || bonus > 1000 || bonus < -1000)
					goto syntax;
			}

			perform_roll(b, prefix, target, sides, dice, bonus);
			c->left++;
			return;
		}
	}

	if (chan == 2 && t > 0) {
		static int64_t last_t;
		if (t - last_t > 3)
			privmsg(b, target, "%s: { I love you! }", prefix);
		last_t = t;
	}
}

void command_hook(struct bio *b, const char *prefix, const char *ident, const char *host,
                  const char *command, const char *const *args, unsigned nargs)
{
	char *buf = NULL;
	int64_t t = -1;
	if (!strcasecmp(command, "NOTICE")) {
		if (nargs < 2 || args[0][0] == '#')
			return;
		if (admin(ident, host))
			b->writeline(b, "%s", args[1]);
		else
			fprintf(stderr, "%s: %s\n", prefix, args[1]);
	} else if (!strcasecmp(command, "JOIN")) {
		t = get_time(b, args[0]);
		rss_check(b, args[0], t);
		command_deliver(b, prefix, args[0]);
		asprintf(&buf, "%"PRIi64",joining %s", t, args[0]);
	} else if (!strcasecmp(command, "PART")) {
		t = get_time(b, args[0]);
		rss_check(b, args[0], t);
		asprintf(&buf, "%"PRIi64",leaving %s", t, args[0]);
	} else if (!strcasecmp(command, "QUIT")) {
		t = get_time(b, NULL);
		asprintf(&buf, "%"PRIi64",quitting with the message \"%s\"", t, args[0]);
	} else if (!strcasecmp(command, "NICK")) {
		t = get_time(b, NULL);
		if (t >= 0) {
			asprintf(&buf, "%"PRIi64",changing nick from %s", t, prefix);
			if (buf)
				update_seen(b, buf, args[0]);
			free(buf);
			buf = NULL;
		}
		asprintf(&buf, "%"PRIi64",changing nick to %s", t, args[0]);
	} else if (atoi(command)) {
		int i;
		fprintf(stderr, ":%s!%s%s %s", prefix, ident, host, command);
		for (i = 0; i < nargs; ++i)
			fprintf(stderr, " %s", args[i]);
		fprintf(stderr, "\n");
	}
	if (t >= 0 && buf)
		update_seen(b, buf, prefix);
	free(buf);
	return;
}

