all: fillybot mod.so

CFLAGS ?= -Wall -Wno-pointer-sign -g -O0 `pkg-config libxml-2.0 --cflags` -Werror -std=gnu99 -fstack-protector-all
LDFLAGS ?= -lssl -lbfd -ldl -Wl,-rpath,\$$ORIGIN -liberty -pie -fPIE
OBJS := main.o crash.o backtrace-symbols.o
MODS := mod.O entities.O
MOD_LDFLAGS ?= -lcurl `pkg-config libxml-2.0 --libs` -lmagic -ltdb
LIBRARY_DEFINES =

-include local.mk

SHARED_HEADERS = shared.h owner.h
MOD_HEADERS = $(SHARED_HEADERS) entities.h mod.h
CORE_HEADERS = $(SHARED_HEADERS) mod.h core.h

owner.h: Makefile
	@echo "static char owner[] = \"`whoami`\";" > owner.h

local.c:
	touch local.c

%.o: %.c $(CORE_HEADERS)
	@echo CC $<
	@$(CC) -c $(INCLUDES) $(CFLAGS) -fPIE $(LIBRARY_DEFINES) $< -o $@

%.o: %.cpp $(CORE_HEADERS)
	@echo CXX $<
	@$(CXX) -c $(INCLUDES) $(CXXFLAGS) -fPIE $(LIBRARY_DEFINES) $< -o $@

%.O: %.c $(MOD_HEADERS)
	@echo CC $<
	@$(CC) -c $(INCLUDES) $(CFLAGS) -Wno-unused-result -fPIC $(LIBRARY_DEFINES) $< -o $@

mod.O:: local.c

fillybot: $(OBJS)
	@echo LD $@
	@$(CC) $(CFLAGS) $(OBJS) $(LDFLAGS) -o $@

mod.so: $(MODS)
	@echo LD $@
	@$(CC) $(CFLAGS) -fPIC -shared $(MODS) $(MOD_LDFLAGS) -o $@

clean:
	rm -f fillybot mod.so *.o *.O *~ owner.h

